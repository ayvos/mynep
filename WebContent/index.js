// index.js

// request message on server
/*xhrGet("api/hello", function(responseText){
	// add to document
	var mytitle = document.getElementById('message');
	mytitle.innerHTML = responseText;

}, function(err){
	console.log(err);
});*/

//utilities
function createXHR(){
	if(typeof XMLHttpRequest != 'undefined'){
		return new XMLHttpRequest();
	}else{
		try{
			return new ActiveXObject('Msxml2.XMLHTTP');
		}catch(e){
			try{
				return new ActiveXObject('Microsoft.XMLHTTP');
			}catch(e){}
		}
	}
	return null;
}
function xhrGet(url, callback){
	var xhr = new createXHR();
	xhr.open("GET", url, true);
	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4){
			if(xhr.status == 200){
				callback(xhr.responseText);
			} else {
				callback('Error on service! Code : '+xhr.status);
			}
		}
	};
	xhr.timeout = 3000;
	xhr.ontimeout = errback;
	xhr.send();
}
function parseJson(str){
	return window.JSON ? JSON.parse(str) : eval('(' + str + ')');
}
function prettyJson(str){
	// If browser does not have JSON utilities, just print the raw string value.
	return window.JSON ? JSON.stringify(JSON.parse(str), null, '  ') : str;
}
function callServlet(url, targetX)
{
    var xmlhttp;
    if (window.XMLHttpRequest)
    {
	    // This part is mainly for the latest browsers which have XMLHttpRequest object
	        // like Chrome,Firefox,Safari and IE7+
	    xmlhttp=new XMLHttpRequest();
    }
    else
    {
	    // This should take care of the older browsers
	    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    
xmlhttp.onreadystatechange=function()
{
/*
readyState has four different states : 
    0: request not initialized 
    1: server connection established
    2: request received 
    3: processing request 
    4: request finished and response is ready
status is ranging between 200 - Ok and 404 - Page Not Found     
*/
	if (xmlhttp.readyState==4 && xmlhttp.status==200)
	{
	    document.getElementById(targetX).innerHTML=xmlhttp.responseText;
	}
}
xmlhttp.open("GET",url,true);
xmlhttp.send();
}
function voteIt(voteVal, voteSubject) {
	var theUrl = "/mynep/vote?sId="+voteSubject+"&vVal="+voteVal;
	callServlet(theUrl, "vote_result"+voteSubject);
	return false;
}

