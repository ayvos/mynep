﻿<%@page import="java.text.DecimalFormat"%>
<%@page import="gov.nasa.events.spaceapps.y2015.mynep.pojos.CrawlResult"%>
<%@page import="gov.nasa.events.spaceapps.y2015.mynep.pojos.CrawlProcessResult"%>
<%@page import="gov.nasa.events.spaceapps.y2015.mynep.pojos.CrawlInput"%>
<%@page import="gov.nasa.events.spaceapps.y2015.mynep.utils.Constants"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Mynep - My Natural Event Platform</title>
        <meta name="description" content="Mynep Project for Spaceapps from Natural Threat Determinors, 2015, ISTANBUL">
        <meta name="author" content="Mynep">
        <meta name="keyword" content="nasa, earth, hazards, mynep">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/fontello.css">
        <link rel="stylesheet" href="css/animate.css">        
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/owl.theme.css">
        <link rel="stylesheet" href="css/owl.transitions.css">
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="responsive.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="js/stars.js"></script>
    </head>
    <script src="../index.js"></script>
	<script>
		function controlFields() {
			var returnValue = false;
			var catId = document.getElementById("t_c").value;
			var monthVal = document.getElementById("t_m").value;
			var yearVal = document.getElementById("t_y").value;
			if (catId != "-1" && monthVal != "-1" && yearVal != "-1") {
				returnValue = true;
			} else {
				var theMsg = "Please correct the errors below:";
				if (catId=="-1") {
					theMsg += "\r\n- Choose an event type";
				}
				if (monthVal=="-1") {
					theMsg += "\r\n- Choose the month value";
				}
				if (yearVal=="-1") {
					theMsg += "\r\n- Choose the year value";
				}
				alert(theMsg);
			}
			return returnValue;
		}
		function htmlEntities(str) {
		    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
		}
	</script>
	<%
		CrawlInput cInput = null;
		if (request.getSession().getAttribute(Constants.HTML_SESSION_CRAWL_INPUT) != null) {
			cInput = (CrawlInput) request.getSession().getAttribute(Constants.HTML_SESSION_CRAWL_INPUT);
		}
		CrawlProcessResult cResult = null;
		if (request.getSession().getAttribute(Constants.HTML_SESSION_CRAWL_PROCESS_RESULT) != null) {
			cResult = (CrawlProcessResult) request.getSession().getAttribute(Constants.HTML_SESSION_CRAWL_PROCESS_RESULT);
		}
	%>
    <body onload="loadStars()">

		<!-- 
        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>
        -->
        <!-- Body content -->
		
        <div class="header-connect">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-8 col-xs-8">
                        <div class="header-half header-call">
                            </div>
                    </div>
                    <div class="col-md-2 col-md-offset-5  col-sm-3 col-sm-offset-1  col-xs-3  col-xs-offset-1">
                        <div class="header-half header-social">
                            <ul class="list-inline">
                                <li><a href="https://www.facebook.com/nasa.mynep" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/nasa_mynep" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <nav class="navbar navbar-default">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="main-nav nav navbar-nav navbar-right">
                <li class="wow fadeInDown" data-wow-delay="0s"><a class="active" href="#">Mynep</a></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>

        <div class="slider-area">
            <div class="slider">
                <div id="bg-slider" class="owl-carousel owl-theme">
                 
                  <div class="item"><img src="img/slider-image-3.jpg"></div>
                  <div class="item"><img src="img/slider-image-2.jpg"></div>
                  <div class="item"><img src="img/slider-image-1.jpg"</div>
                 
                </div>
            </div>
            <div class="container slider-content">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                        <h2>Explore Earth Hazards</h2>
                        <div class="search-form wow pulse" data-wow-delay="0.8s">
                        	<form action="/mynep/main" method="post" onsubmit="return controlFields();" class="form-inline">
                        		<input type="hidden" name="wf" id="wf" value="site">
                                <div class="form-group">
                                    
                                </div>
                                <div class="form-group">
                                    <select name="t_c" id="t_c" class="form-control">
                                        <option value="-1" selected>Event Type</option>
                                        <option value="1" <% if (cInput != null && cInput.getCategoryId().equals("1")) { %> selected <% } %>>Atmosphere</option>
										<option value="2" <% if (cInput != null && cInput.getCategoryId().equals("2")) { %> selected <% } %>>Heat</option>
										<option value="3" <% if (cInput != null && cInput.getCategoryId().equals("3")) { %> selected <% } %>>Land</option>
										<option value="4" <% if (cInput != null && cInput.getCategoryId().equals("4")) { %> selected <% } %>>Life</option>
										<option value="5" <% if (cInput != null && cInput.getCategoryId().equals("5")) { %> selected <% } %>>Water</option>
										<option value="6" <% if (cInput != null && cInput.getCategoryId().equals("6")) { %> selected <% } %>>Snow & Ice</option>
										<option value="15" <% if (cInput != null && cInput.getCategoryId().equals("15")) { %> selected <% } %>>Human Presence</option>
										<option value="96" <% if (cInput != null && cInput.getCategoryId().equals("96")) { %> selected <% } %>>Remote Sensing</option>
										<option value="13" <% if (cInput != null && cInput.getCategoryId().equals("13")) { %> selected <% } %>>Biography</option>
										<option value="1465" <% if (cInput != null && cInput.getCategoryId().equals("1465")) { %> selected <% } %>>Fact Sheets</option>
										<option value="1475" <% if (cInput != null && cInput.getCategoryId().equals("1475")) { %> selected <% } %>>Interviews</option>
										<option value="1522" <% if (cInput != null && cInput.getCategoryId().equals("1522")) { %> selected <% } %>>Galleries</option>
										<option value="1567" <% if (cInput != null && cInput.getCategoryId().equals("1567")) { %> selected <% } %>>World of Change</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select name="t_m" id="t_m" class="form-control">
                                        <option value="-1" selected>Month</option>
			                            <% 
											String val = "";
											for (int i = 1;i<=12;i++) {
												val = "" + i;
												if (val.length() < 2) {
													val = "0" + val;
												}
												%>
													<option value="<%=val%>" <% if (cInput != null && cInput.getMonth().equals(val)) { %> selected <% } %>><%=val%></option>
												<%
											}
										%>
                                    </select>
                                </div>
                                <div class="form-group">
                                  <select name="t_y" id="t_y" class="form-control">
                                    <option value="-1" selected>Year</option>
                                   <%
										int year = Calendar.getInstance().get(Calendar.YEAR);
										for (int j = 2002;j<=year; j++) {
											%>
											<option value="<%=j%>" <% if (cInput != null && cInput.getYear().equals(""+j)) { %> selected <% } %>><%=j%></option>
											<%
										}
									%>
                                  </select>
                                </div>
								<input type="submit" class="btn" value="Search">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>		
		
        <div class="content-area">
        	<div class="container">
        	<%
        	try {
			request.getSession().setAttribute(Constants.HTML_SESSION_CRAWL_INPUT, null); // Resetting the input after rendering...
			if (cResult != null) {
				String messageText = "";
				if (cResult.getResultCode() == Constants.RETCODE_SUCCESS) {
					messageText = "Congrats. I found " + cResult.getResultList().size() + " results for you.";
				} else if (cResult.getResultCode() == Constants.RETCODE_NODATA) {
					messageText = "I am very sorry but i couldn't find ay results. Try another, please.";
				} else {
					messageText = "Ohh Cosmos! A problem has occurred. Detail : " + cResult.getResultMsg();
				}
			%>
                <div class="row page-title text-center wow zoomInDown" data-wow-delay="1s" id="resultdiv">
                    <h5>Your Process is completed @ <%=Calendar.getInstance().getTime().toString()%></h5>
                    <h2>Mynep is saying that...</h2>
                    <p>"<b><%=messageText%></b>"</p>
                </div>
                <%
				String theLink = "";
                int htmlIdentityIndex = -1;
				for (CrawlResult tRes : cResult.getResultList()) {
					++htmlIdentityIndex;
					theLink = Constants.NASA_SITE_PREFIX + tRes.getResultUrl();
					%>
					<div class="row how-it-work text-center">
	                    <div class="col-md-4">
	                        <div class="wow fadeInUp" data-wow-delay="0.8s">
	                            <a href="<%=theLink%>" target="_blank" title="Go to detail of <%=tRes.getResultCoreTxt()%>"><img src="<%=tRes.getResultImageUrl()%>" alt="<%=tRes.getResultCoreTxt()+", thanks to Nasa...Mynpe Team"%>"></a>
	                            <h3><a href="<%=theLink%>" title="Go to detail of <%=tRes.getResultCoreTxt()%>"><%=tRes.getResultCoreTxt()%></a></h3>
	                            <p><%=tRes.getResultRecordDate()%></p>
	                        </div>
	                    </div>
	                    <div class="col-md-4">
	                        <div class="wow fadeInUp"  data-wow-delay="0.9s" id="g_content<%=htmlIdentityIndex%>">
	                        </div>
	                    </div>
	                    <%
							String theBaseURL = "https://www.googleapis.com/customsearch/v1?num=3&key=AIzaSyBirBpy73-onJukdQkfvUPwPYUAEyKZJho&amp;cx=014940472615552832484:in_xhkelxvm&amp;q=";
							String theCallbackMethodName = "hndlr" + htmlIdentityIndex;
							String doubleQuoteQueryStr = "'" + tRes.getResultCoreTxt() + "'";
							theBaseURL += doubleQuoteQueryStr + "&amp;callback=" + theCallbackMethodName;
						%>
						<script>
							function hndlr<%=htmlIdentityIndex%>(response) {
							 var itemLength = response.items.length;
							 var headerTxt = "<center><img src='img/how-work2.png' width='50' height='50'></center><br>";
							 headerTxt += "<font color='#0022FF'>Top <b>"+itemLength+"</b> results</font>";
							 var visText = "<table border='0'><th colspan='2' align='center'>"+headerTxt+"</th>";
							 visText += "<tr><td><font color='FF0000'><b><u>Source</u></b></font></td><td><font color='FF0000'><b><u>Link</u></b></font></td></tr>";
	 					      for (var i = 0; i < response.items.length; i++) {
	 					        var item = response.items[i];
	 					        // TODO : in production code, item.htmlTitle should have the HTML entities escaped.
	 					        var tSource = item.displayLink;
	 					        var tUrl = "<a href='"+item.link+"' target='_blank' title='"+item.snippet+"'><font color='#00BBF4'><b>"+item.title+"</b></font></a>";
	 					        visText += "<tr><td>"+tSource+"</td><td>"+tUrl+"</td></tr>";
	 					      }
	 					      visText += "</table>";
	 					     document.getElementById("g_content<%=htmlIdentityIndex%>").innerHTML = visText;
	 					    }
	 					</script>
	 					<script src="<%=theBaseURL%>"></script>	
	                    <div class="col-md-4">
	                        <div class="wow fadeInUp"  data-wow-delay="1s" id="y_content<%=htmlIdentityIndex%>">
	                        	<script>
									var aUrl = "/mynep/video?qTxt=<%=tRes.getResultCoreTxt()%>";
									callServlet(aUrl, "y_content<%=htmlIdentityIndex%>");
			 					</script>
	                        </div>
	                    </div>
	                </div>
	                 <div class="container">
		                <div class="row page-title text-center wow bounce"  data-wow-delay="1s">
		                    <h2>Popularity of <span><%=tRes.getResultCoreTxt()%></span></h2>
		                </div>
		                <div class="row jobs">
		                    <div>
		                        <div class="job-posts table-responsive">
		                            <table class="table">
		                                <tr class="odd wow fadeInUp" data-wow-delay="1s">
		                                    <td class="tbl-logo"><img src="img/job-logo1.png" alt=""></td>
		                                    <td class="tbl-title" id="vote_result<%=tRes.getResultUniqueIdentifier()%>"><h4><%=tRes.getVoteExpStr()%><br><font size='+1'><b><%=new DecimalFormat("##.##").format(tRes.getVoteInfo().getVoteGeneralVal())%></b></font></h4></td>
		                                    <td><p>So, what is your vote?</p></td>
		                                    <td>
		                                    	<img src="img/star1.gif" onmouseover="highlight(this.id)" onclick="setStar(this.id);voteIt('1', '<%=tRes.getResultUniqueIdentifier()%>');" onmouseout="losehighlight(this.id)" id="<%=tRes.getResultUniqueIdentifier()%>_1" style="width:30px; height:30px; float:left;" />
												<img src="img/star1.gif" onmouseover="highlight(this.id)" onclick="setStar(this.id);voteIt('2', '<%=tRes.getResultUniqueIdentifier()%>');" onmouseout="losehighlight(this.id)" id="<%=tRes.getResultUniqueIdentifier()%>_2" style="width:30px; height:30px; float:left;" />
												<img src="img/star1.gif" onmouseover="highlight(this.id)" onclick="setStar(this.id);voteIt('3', '<%=tRes.getResultUniqueIdentifier()%>');" onmouseout="losehighlight(this.id)" id="<%=tRes.getResultUniqueIdentifier()%>_3" style="width:30px; height:30px; float:left;" />
												<br>
												<div id="vote<%=tRes.getResultUniqueIdentifier()%>"></div>
		                                    </td>
		                                    <td class="tbl-apply">Click a star to vote</a></td>
		                                </tr>
		                            </table>
		                        </div>
		                    </div>
		                </div>
		            </div>
					<%
				}
				%>
				<script>
					window.location.hash = '#resultdiv';
                </script>
				<%
				request.getSession().setAttribute(Constants.HTML_SESSION_CRAWL_PROCESS_RESULT, null); // Resetting the putput after rendering...
			} else {
				%>
					 <div class="row page-title text-center wow zoomInDown" data-wow-delay="1s">
	                    <h5>Our Process is EARTH</h5>
	                    <h2>Welcome to Beautiful World's Amazing Volcanoes, Icebergs and Cats from Space</h2>
	                 </div>
	                 <div class="row page-title wow zoomInDown" data-wow-delay="1s">
	                    <p>
	                       <b>MyNEP</b> solves tens of issues that the students, professors and any kind of researchers are facing with. <br>
	                       People will be able to reach the data from an individual source. 
	                       <b>MyNEP</b> allows users to see the data from an official NASA Observatory and relating them with Google and Youtube search engines through online APIs. <br>
	                       So that, researchers will not spend too much time to find the specific sources from diverse places. <br>
	                       <b>MyNEP</b> gives them opportunity to categorise the natural hazards with a specific type of hazard, specific year and specific month.
	                    </p>
	                </div>
				<%
			}
        	} catch (Exception e) {
				request.getSession().setAttribute(Constants.HTML_SESSION_CRAWL_PROCESS_RESULT, null); // Resetting the putput after rendering...
			}
            %>
            </div>
            <hr>

            <div class="container">
                <div class="row page-title text-center  wow bounce"  data-wow-delay=".7s">
                    <h5>DISCUSSIONS</h5>
                    <h2>WHAT PEOPLES ARE SAYING ABOUT HAZARDS</h2>
                </div>
                <div class="row testimonial">
                    <div class="col-md-12">
                        <div id="testimonial-slider">
                            <div class="item">
                                <div class="client-text">                                
                                    <p>Fires are dangerous and also hot.</p>
                                    <h4><strong>Hangul Eray, </strong><i>Software Developer</i></h4>
                                </div>
                                <div class="client-face wow fadeInRight" data-wow-delay=".9s"> 
                                    <img src="img/client-face1.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="client-text">                                
                                    <p>Storms are exicing</p>
                                    <h4><strong>Bal Canberk, </strong><i>Student</i></h4>
                                </div>
                                <div class="client-face">
                                    <img src="img/client-face2.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="client-text">                                
                                    <p>Atmosphere's blue color is the most beautiful thing on the woorld!</p>
                                    <h4><strong>Yavas Mehmet, </strong><i>Telecom Expert</i></h4>
                                </div>
                                <div class="client-face">
                                    <img src="img/client-face1.png" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <div class="client-text">                                
                                    <p>I like earth too much! I can eat it..:)</p>
                                    <h4><strong>Yuksel Veysel, </strong><i>Computer Expert</i></h4>
                                </div>
                                <div class="client-face">
                                    <img src="img/client-face2.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="footer-area">
            <div class="container">
                <div class="row footer">
                    <div class="col-md-4">
                        <div class="single-footer">
                            <img src="img/footer-logo.jpg" alt="" class="wow pulse" data-wow-delay="1s">
                            <p>Mynep aims to guide the people in order to sustain a social developing forecasting platform, which allows them to be informed easily.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-footer">
                            <h4>Mynep Twitter Update</h4>
                            <a class="twitter-timeline"  href="https://twitter.com/nasa_mynep" data-widget-id="587002394577412096">Tweets by @nasa_mynep</a>
            				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-footer">
                            <h4>Useful lnks</h4>
                            <div class="footer-links">
                                <ul class="list-unstyled">
                                    <li><a href="https://2015.spaceappschallenge.org/project/mynep-my-natural-event-platform/" target="_blank">About Us</a></li>
                                    <li><a href="https://twitter.com/nasa_mynep" target="_blank">Contact Us</a></li>
                                    <li><a href="http://earthobservatory.nasa.gov/Images/" target="_blank">Earth Observer</a></li>
                                    <li><a href="https://2015.spaceappschallenge.org/" target="_blank">2015 Spaceapps Event</a></li>
                                    <li><a href="https://twitter.com/spaceapps" target="_blank">@spaceapps by Twitter</a></li>
                                    <li><a href="https://www.facebook.com/spaceappschallenge" target="_blank">Spaceapps on Facebook</a></li>
                                    <li><a href="http://www.nasa.gov/" target="_blank">USA National Space Agency - NASA</a></li>
                                    <li><a href="http://en.federalspace.ru/" target="_blank">Russian Federal Space Agency</a></li>
                                    <li><a href="http://www.esa.int/" target="_blank">European Space Agency</a></li>
									<li><a href="http://global.jaxa.jp/" target="_blank">Japan Space Agency</a></li>
									<li><a href="http://www.tubitak.gov.tr/en" target="_blank">Tubitak of Turkey</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row footer-copy">
                    <p><span>(C) website, All rights reserved</span> | <span>Re-Designed by</span> <a href="https://www.facebook.com/nasa.mynep" target="_blank">Mynep Team</a> </p>
                </div>
            </div>
        </div>
		
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/wow.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
