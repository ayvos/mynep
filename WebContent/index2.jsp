<%@page import="gov.nasa.events.spaceapps.y2015.mynep.pojos.CrawlResult"%>
<%@page import="gov.nasa.events.spaceapps.y2015.mynep.pojos.CrawlProcessResult"%>
<%@page import="gov.nasa.events.spaceapps.y2015.mynep.pojos.CrawlInput"%>
<%@page import="gov.nasa.events.spaceapps.y2015.mynep.utils.Constants"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Mynep Project for Spaceapps from Natural Threat Determinors, 2015, ISTANBUL</title>
</head>
<script src="index.js"></script>
<script>
	function controlFields() {
		var returnValue = false;
		var catId = document.getElementById("t_c").value;
		var monthVal = document.getElementById("t_m").value;
		var yearVal = document.getElementById("t_y").value;
		if (catId != "-1" && monthVal != "-1" && yearVal != "-1") {
			returnValue = true;
		} else {
			var theMsg = "Please correct the errors below:";
			if (catId=="-1") {
				theMsg += "\r\n- Choose an event type";
			}
			if (monthVal=="-1") {
				theMsg += "\r\n- Choose the month value";
			}
			if (yearVal=="-1") {
				theMsg += "\r\n- Choose the year value";
			}
			alert(theMsg);
		}
		return returnValue;
	}
	function htmlEntities(str) {
	    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
	}
</script>
<%
	CrawlInput cInput = null;
	if (request.getSession().getAttribute(Constants.HTML_SESSION_CRAWL_INPUT) != null) {
		cInput = (CrawlInput) request.getSession().getAttribute(Constants.HTML_SESSION_CRAWL_INPUT);
	}
	CrawlProcessResult cResult = null;
	if (request.getSession().getAttribute(Constants.HTML_SESSION_CRAWL_PROCESS_RESULT) != null) {
		cResult = (CrawlProcessResult) request.getSession().getAttribute(Constants.HTML_SESSION_CRAWL_PROCESS_RESULT);
	}
%>
<body>
	<form action="/mynep/main" method="post" onsubmit="return controlFields();">
		<h1><font color="#FF0000"><b>Please choose a date and category to see the events occurred on earth!</b></font></h1>
		<br>
		<table border="0">
			<tr>
				<td>
					<b><font color="#FF0000">*</font>Event Type</b>
				</td>
				<td>
					<select name="t_c" id="t_c">
						<option value="-1" selected>Please choose</option>
						<option value="1" <% if (cInput != null && cInput.getCategoryId().equals("1")) { %> selected <% } %>>Atmosphere</option>
						<option value="2" <% if (cInput != null && cInput.getCategoryId().equals("2")) { %> selected <% } %>>Heat</option>
						<option value="3" <% if (cInput != null && cInput.getCategoryId().equals("3")) { %> selected <% } %>>Land</option>
						<option value="4" <% if (cInput != null && cInput.getCategoryId().equals("4")) { %> selected <% } %>>Life</option>
						<option value="5" <% if (cInput != null && cInput.getCategoryId().equals("5")) { %> selected <% } %>>Water</option>
						<option value="6" <% if (cInput != null && cInput.getCategoryId().equals("6")) { %> selected <% } %>>Snow & Ice</option>
						<option value="15" <% if (cInput != null && cInput.getCategoryId().equals("15")) { %> selected <% } %>>Human Presence</option>
						<option value="96" <% if (cInput != null && cInput.getCategoryId().equals("96")) { %> selected <% } %>>Remote Sensing</option>
						<option value="13" <% if (cInput != null && cInput.getCategoryId().equals("13")) { %> selected <% } %>>Biography</option>
						<option value="1465" <% if (cInput != null && cInput.getCategoryId().equals("1465")) { %> selected <% } %>>Fact Sheets</option>
						<option value="1475" <% if (cInput != null && cInput.getCategoryId().equals("1475")) { %> selected <% } %>>Interviews</option>
						<option value="1522" <% if (cInput != null && cInput.getCategoryId().equals("1522")) { %> selected <% } %>>Galleries</option>
						<option value="1567" <% if (cInput != null && cInput.getCategoryId().equals("1567")) { %> selected <% } %>>World of Change</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<b><font color="#FF0000">*</font>Month</b>
				</td>
				<td>
					<select name="t_m" id="t_m">
						<option value="-1">Please Choose</option>
						<% 
							String val = "";
							for (int i = 1;i<=12;i++) {
								val = "" + i;
								if (val.length() < 2) {
									val = "0" + val;
								}
								%>
									<option value="<%=val%>" <% if (cInput != null && cInput.getMonth().equals(val)) { %> selected <% } %>><%=val%></option>
								<%
							}
						%>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<b><font color="#FF0000">*</font>Year</b>
				</td>
				<td>
					<select name="t_y" id="t_y">
						<option value="-1">Please Choose</option>
						<%
							int year = Calendar.getInstance().get(Calendar.YEAR);
							for (int j = 2002;j<=year; j++) {
								%>
								<option value="<%=j%>" <% if (cInput != null && cInput.getYear().equals(""+j)) { %> selected <% } %>><%=j%></option>
								<%
							}
						%>
					</select>
				</td>
			</tr>
		</table><br>
		<input type="submit" value="See the Natural Events">
		<%
			request.getSession().setAttribute(Constants.HTML_SESSION_CRAWL_INPUT, null); // Resetting the input after rendering...
			if (cResult != null) {
				int tColspan = 5;
				%>
					<br>
					<table border="1">
						<th colspan="<%=tColspan%>" align="center"><b>Results for criterias @ <%=Calendar.getInstance().getTime().toString()%></b></th>
						<%
							String theColorCode = "#00FF00";
							int itemCount = 0;
							int htmlIdentityIndex = -1;
							if (cResult.getResultCode() == Constants.RETCODE_SUCCESS) {
								itemCount = cResult.getResultList().size();
								%>
								<tr><td colspan="<%=tColspan%>"><font color="<%=theColorCode%>" size="+1"><b><%=cResult.getResultMsg()%></b></td></tr>
								<tr>
									<td><b>Date</b></td>
									<td><b>Description</b></td>
									<td><b>Preview</b></td>
									<td><b>Popular Related Web Results</b></td>
									<td><b>Youtube Related Results</b></td>
								</tr>
								<%
								String theLink = "";
								for (CrawlResult tRes : cResult.getResultList()) {
									++htmlIdentityIndex;
									theLink = Constants.NASA_SITE_PREFIX + tRes.getResultUrl();
									%>
									<tr>
										<td><%=tRes.getResultRecordDate()%></td>
										<td><a href="<%=theLink%>" target="_blank"><font color="#FF9900"><b><%=tRes.getResultCoreTxt()%></b></font></a></td>
										<td><img src="<%=tRes.getResultImageUrl()%>" alt="<%=tRes.getResultCoreTxt()+", thanks to Nasa...Mynpe Team"%>"/></td>
										<td id="g_content<%=htmlIdentityIndex%>">
											Getting related web results for <b><%=tRes.getResultCoreTxt()%></b>...
											<%
												String theBaseURL = "https://www.googleapis.com/customsearch/v1?key=AIzaSyBirBpy73-onJukdQkfvUPwPYUAEyKZJho&amp;cx=014940472615552832484:in_xhkelxvm&amp;q=";
												String theCallbackMethodName = "hndlr" + htmlIdentityIndex;
												String doubleQuoteQueryStr = "'" + tRes.getResultCoreTxt() + "'";
												theBaseURL += doubleQuoteQueryStr + "&amp;callback=" + theCallbackMethodName;
											%>
											<script>
												function hndlr<%=htmlIdentityIndex%>(response) {
												 var itemLength = response.items.length;
												 var headerTxt = "<font color='#0022FF'>Top <b>"+itemLength+"</b> results</font>";
												 var visText = "<table border='0'><th colspan='2' align='center'>"+headerTxt+"</th>";
												 visText += "<tr><td><font color='FF0000'><b><u>Source</u></b></font></td><td><font color='FF0000'><b><u>Link</u></b></font></td></tr>";
						 					      for (var i = 0; i < response.items.length; i++) {
						 					        var item = response.items[i];
						 					        // TODO : in production code, item.htmlTitle should have the HTML entities escaped.
						 					        var tSource = item.displayLink;
						 					        var tUrl = "<a href='"+item.link+"' target='_blank' title='"+item.snippet+"'><font color='#00BBF4'><b>"+item.title+"</b></font></a>";
						 					        visText += "<tr><td>"+tSource+"</td><td>"+tUrl+"</td></tr>";
						 					      }
						 					      visText += "</table>";
						 					     document.getElementById("g_content<%=htmlIdentityIndex%>").innerHTML = visText;
						 					    }
						 					</script>
						 					<script src="<%=theBaseURL%>"></script>											
										</td>
										<td id="y_content<%=htmlIdentityIndex%>">
						 					<script>
<%-- 							 					function yvhndlr<%=htmlIdentityIndex%>(response) { --%>
<%-- 							 						var targetX = "y_content<%=htmlIdentityIndex%>"; --%>
// 							 						document.getElementById(targetX).innerHTML = response;
// 							 					}
<%-- 						 					    var aUrl = "http://mynep.eu-gb.mybluemix.net/mynep/video?qTxt=<%=tRes.getResultCoreTxt()%>"; --%>
<%-- 						 					    xhrGet(aUrl, "yvhndlr<%=htmlIdentityIndex%>"); --%>
												var aUrl = "/mynep/video?qTxt=<%=tRes.getResultCoreTxt()%>";
												callServlet(aUrl, "y_content<%=htmlIdentityIndex%>");
						 					</script>		
										</td>
									</tr>
									<%
								}
								%>
								<tr><td colspan="<%=tColspan%>"><b>Total Result(s) : <%=cResult.getResultList().size()%></b></td></tr>
								<%
							} else {
								theColorCode = "#0000FF";
								if (cResult.getResultCode() == Constants.RETCODE_FAIL) {
									theColorCode = "#FF0000";
								}
								%>
									<tr><td colspan="<%=tColspan%>"><font color="<%=theColorCode%> size="+1"><%=cResult.getResultMsg()+""%></font></td></tr>
								<%
							}
						%>
					</table>
				<%
				request.getSession().setAttribute(Constants.HTML_SESSION_CRAWL_PROCESS_RESULT, null); // Resetting the putput after rendering...
			}
		%>
	</form>
</body>
</html>