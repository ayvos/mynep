package gov.nasa.events.spaceapps.y2015.mynep.servlet;

import gov.nasa.events.spaceapps.y2015.mynep.impl.NasaDataCrawler;
import gov.nasa.events.spaceapps.y2015.mynep.pojos.CrawlInput;
import gov.nasa.events.spaceapps.y2015.mynep.pojos.CrawlProcessResult;
import gov.nasa.events.spaceapps.y2015.mynep.utils.Constants;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MainCrawlerServlet
 */
@WebServlet("/MainCrawlerServlet")
public class MainCrawlerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MainCrawlerServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Get the parameters and do the crawl process...
		NasaDataCrawler dataCrawler = new NasaDataCrawler();
		CrawlInput cInput = new CrawlInput();
		cInput.setCategoryId(request.getParameter(Constants.HTML_FORM_PARAM_CATEGORY).trim());
		cInput.setMonth(request.getParameter(Constants.HTML_FORM_PARAM_MONTH).trim());
		cInput.setYear(request.getParameter(Constants.HTML_FORM_PARAM_YEAR).trim());
		request.getSession().setAttribute(Constants.HTML_SESSION_CRAWL_INPUT, cInput);
		CrawlProcessResult cResult = dataCrawler.doCrawlProcess(cInput);
		request.getSession().setAttribute(Constants.HTML_SESSION_CRAWL_PROCESS_RESULT, cResult);
		String rePath = request.getParameter("wf");
		if (rePath != null && rePath.trim().length() > 0) {
			rePath = "/" + rePath;
		} else {
			rePath = "/";
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(rePath);
		dispatcher.forward(request, response);
		//response.sendRedirect(rePath);
	}

}
