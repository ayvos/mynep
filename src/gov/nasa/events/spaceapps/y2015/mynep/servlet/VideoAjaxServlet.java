package gov.nasa.events.spaceapps.y2015.mynep.servlet;

import gov.nasa.events.spaceapps.y2015.mynep.impl.VideoCrawler;
import gov.nasa.events.spaceapps.y2015.mynep.pojos.VideoProcessResult;
import gov.nasa.events.spaceapps.y2015.mynep.pojos.VideoResultPojo;
import gov.nasa.events.spaceapps.y2015.mynep.utils.Constants;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class VideoAjaxServlet
 */
@WebServlet("/VideoAjaxServlet")
public class VideoAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VideoAjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Get the queryKey and return the result as html table content string...
		response.setContentType("text/html");
		String queryTxt = request.getParameter("qTxt");
		String theText = "";
		System.out.println("Query : " + queryTxt);
		if (queryTxt != null && queryTxt.trim().length() > 0) {
			VideoCrawler vC = new VideoCrawler();
			VideoProcessResult result = vC.getVideoResult(queryTxt);
			if (result.getResultCode() == Constants.RETCODE_SUCCESS) {
				List<VideoResultPojo> vList = result.getVideoResultList();
				theText = "<center><img src='img/how-work3.png' width='50' height='50'></center><br><table>";
				for (VideoResultPojo videoResultPojo : vList) {
					theText += "<tr>";
					theText += "<td>";
					theText += "<h3>" + videoResultPojo.getVideoTitle() + "</h3><br>";
					theText += "<a href="+Constants.GAPI_WATCH_SITE_PREVIX + videoResultPojo.getVideoId() + " title = 'Watch " + videoResultPojo.getVideoTitle() + "' target='_blank'><img src='" + videoResultPojo.getThumbnailPath() + "' title='Watch " + videoResultPojo.getVideoTitle() +"'/></a>";
					theText += "</td>";
					theText += "</tr>";
				}
				theText += "</table>";
			} else if (result.getResultCode() == Constants.RETCODE_FAIL) {
				theText = "<b>" + result.getResultMsg() + "</b>";
			}
			System.out.println("The text : " + theText);
			response.getWriter().print(theText);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
