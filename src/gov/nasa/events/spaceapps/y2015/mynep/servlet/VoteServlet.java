package gov.nasa.events.spaceapps.y2015.mynep.servlet;

import gov.nasa.events.spaceapps.y2015.mynep.impl.VoteDAO;
import gov.nasa.events.spaceapps.y2015.mynep.pojos.VotePojo;
import gov.nasa.events.spaceapps.y2015.mynep.utils.Constants;

import java.io.IOException;
import java.text.DecimalFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class VoteServlet
 */
@WebServlet("/VoteServlet")
public class VoteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VoteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String subjectId = request.getParameter(Constants.HTML_FORM_PARAM_SUBJECT_ID);
		String voteVal = request.getParameter(Constants.HTML_FORM_PARAM_VOTE_VALUE);
		if (voteVal != null && subjectId != null) {
			VotePojo vPojo = new VotePojo();
			vPojo.setUniquIdentifierTxt(subjectId);
			vPojo.setVoteGeneralVal(Double.parseDouble(voteVal));
			VoteDAO vDao = new VoteDAO();
			try {
				VotePojo retVal = vDao.recordNewVote(vPojo);
				String theText = "<h4>"+retVal.getVoteExpr()+" <br><font size='+1'><b>"+new DecimalFormat("##.##").format(retVal.getVoteGeneralVal())+"</b></font></h4>";
				response.setContentType("text/html");
				response.getWriter().print(theText);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
