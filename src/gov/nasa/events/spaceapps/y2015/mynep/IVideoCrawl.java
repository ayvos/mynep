package gov.nasa.events.spaceapps.y2015.mynep;

import gov.nasa.events.spaceapps.y2015.mynep.pojos.VideoProcessResult;

public interface IVideoCrawl {

	public VideoProcessResult getVideoResult(String qKey);
}
