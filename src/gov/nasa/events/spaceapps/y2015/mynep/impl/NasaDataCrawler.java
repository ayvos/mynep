package gov.nasa.events.spaceapps.y2015.mynep.impl;

import gov.nasa.events.spaceapps.y2015.mynep.ICrawler;
import gov.nasa.events.spaceapps.y2015.mynep.pojos.CrawlInput;
import gov.nasa.events.spaceapps.y2015.mynep.pojos.CrawlProcessResult;
import gov.nasa.events.spaceapps.y2015.mynep.pojos.CrawlResult;
import gov.nasa.events.spaceapps.y2015.mynep.pojos.CrawlResultType;
import gov.nasa.events.spaceapps.y2015.mynep.utils.Constants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class NasaDataCrawler implements ICrawler {

	public NasaDataCrawler(String theCrawlUrl) {
		this.theCrawlUrl = theCrawlUrl;
		if (this.theCrawlUrl == null || this.theCrawlUrl.trim().length() == 0) {
			this.theCrawlUrl = Constants.NASA_SITE_PREFIX + Constants.NASA_SITE_PREFIX_IMAGES;
		}
	}
	
	public NasaDataCrawler() {
	}

	@Override
	public CrawlProcessResult doCrawlProcess(CrawlInput cInput) {
		CrawlProcessResult returnValue = new CrawlProcessResult();
		Document doc;
		try {
			// need http protocol
			String theUrlToBeConnect = createUrl(cInput);
			doc = Jsoup.connect(theUrlToBeConnect).get();
			// get page title
			String title = doc.title();
			System.out.println("title : " + title);
			Elements coreGridElement = doc.getElementsByAttributeValueContaining("class", "grid-mid");
			if (coreGridElement != null && coreGridElement.size() > 0) {
				List<CrawlResult> cList = new ArrayList<CrawlResult>();
				for (Element coreElement : coreGridElement) {
					if (coreElement != null && coreElement.childNodeSize() > 0) {
						// Get natural hazard images...
						Elements naturalHazardElements = coreElement.getElementsByAttributeValueContaining("href", Constants.NASA_SITE_PREFIX_HTML_CONTENT_NATURAL_HAZARD);
						if (naturalHazardElements != null && !naturalHazardElements.isEmpty()) {
							for (Element naturalHazardElement : naturalHazardElements) {
								CrawlResult cResult = new CrawlResult();
								cResult.setResultType(CrawlResultType.HAZARD_IMAGE);
								Elements nheLinks = naturalHazardElement.getElementsByTag("a");
								if (nheLinks != null && !nheLinks.isEmpty()) {
									Element nheLink = nheLinks.get(0);
									cResult.setResultUrl(nheLink.attr("href"));
									System.out.println(cResult.getResultUrl());
									cResult.setResultImageUrl(nheLink.childNodes().get(0).attr("src"));
									System.out.println("Result image url : " + cResult.getResultImageUrl());
									int existenceIndex = isCrawlResultExist(cResult, cList);
									if (existenceIndex < 0) {
										cList.add(cResult);
									} else {
										// It is the text explanation!
										cList.get(existenceIndex).setResultCoreTxt(nheLink.html());
										System.out.println("Result core txt : " + nheLink.html());
									}
									cResult.setResultRecordDate(getDateOfEvent(cResult, coreElement.getElementsByTag("div").html()));
								}
							}
						}
						// Get today images...
						Elements todayElements = coreElement.getElementsByAttributeValueContaining("href", Constants.NASA_SITE_PREFIX_HTML_CONTENT_TODAY_IMAGE);
						if (todayElements != null && !todayElements.isEmpty()) {
							for (Element todayElement : todayElements) {
								CrawlResult cResult = new CrawlResult();
								cResult.setResultType(CrawlResultType.TODAY_IMAGE);
								Elements nheLinks = todayElement.getElementsByTag("a");
								if (nheLinks != null && !nheLinks.isEmpty()) {
									Element nheLink = nheLinks.get(0);
									cResult.setResultUrl(nheLink.attr("href"));
									System.out.println(cResult.getResultUrl());
									cResult.setResultImageUrl(nheLink.childNodes().get(0).attr("src"));
									System.out.println("Result image url : " + cResult.getResultImageUrl());
									int existenceIndex = isCrawlResultExist(cResult, cList);
									if (existenceIndex < 0) {
										cList.add(cResult);
									} else {
										// It is the text explanation!
										cList.get(existenceIndex).setResultCoreTxt(nheLink.html());
										System.out.println("Result core txt : " + nheLink.html());
									}
									cResult.setResultRecordDate(getDateOfEvent(cResult, coreElement.getElementsByTag("div").html()));
								}
							}
						}
					}
				}
				if (cList != null && !cList.isEmpty()) {
					returnValue.setResultMsg("Process is successful");
					returnValue.setResultCode(Constants.RETCODE_SUCCESS);
				} else {
					returnValue.setResultMsg("No data found");
					returnValue.setResultCode(Constants.RETCODE_NODATA);
				}
				returnValue.setResultList(cList);
			}	 
		} catch (IOException e) {
			returnValue.setResultCode(Constants.RETCODE_FAIL);
			returnValue.setResultMsg("Exception occurred. Detail : " + e.getMessage());
			e.printStackTrace();
		} finally {
			// Get the vote infos of related results...
			VoteDAO vd = new VoteDAO();
			try {
				returnValue.setResultList(vd.getBulkVoteList(returnValue.getResultList()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return returnValue;
	}
		
	// Accessor methods...
	public String getTheCrawlUrl() {
		return theCrawlUrl;
	}
	
	// Method to control about duplicate url existence...
	private int isCrawlResultExist(CrawlResult comingResult, List<CrawlResult> theList) {
		int returnValue = -1;
		if (theList != null && !theList.isEmpty()) {
			CrawlResult crawlResult = null;
			for (int i = 0;i<=theList.size() - 1;i++) {
				crawlResult = theList.get(i);
				if (crawlResult.equals(comingResult)) {
					returnValue = i;
					break;
				}
			}
		}
		return returnValue;
	}
	
	// Date extractor...
	private String getDateOfEvent(CrawlResult comingRes, String theHtmlText) {
		String returnValue = "";
		int firstIndicator = theHtmlText.indexOf(comingRes.getResultUrl());
		int theIndexOfStart = theHtmlText.indexOf("</a>", firstIndicator);
		int theIndexOfFinish = theHtmlText.indexOf("<br>", firstIndicator);
		if (theIndexOfStart > 0 && theIndexOfFinish > 0 && theIndexOfFinish > theIndexOfStart) {
			returnValue = theHtmlText.substring(theIndexOfStart + 4, theIndexOfFinish);
		}
		return returnValue;
	}
	
	// Connect url creator...
	private String createUrl(CrawlInput cInput) {
		String retVal = "";
		retVal = Constants.NASA_SITE_PREFIX + 
				Constants.NASA_SITE_PREFIX_IMAGES + 
				"?" +
				Constants.NASA_SITE_PREFIX_PARAM_CATEGORY +
				"=" + 
				cInput.getCategoryId() + 
				"&" +
				Constants.NASA_SITE_PREFIX_PARAM_MONTH +
				"=" + 
				cInput.getMonth() + 
				"&" +
				Constants.NASA_SITE_PREFIX_PARAM_YEAR +
				"=" + 
				cInput.getYear();
		return retVal;
	}

	// Fields...
	private String theCrawlUrl;
	
	public static void main(String[] args) {
		String theUrl = "";
		if (args != null && args.length == 1) {
			theUrl = args[0];
		}
		NasaDataCrawler nc = new NasaDataCrawler(theUrl);
		CrawlInput cInput = new CrawlInput();
		CrawlProcessResult theResult = nc.doCrawlProcess(cInput);
		if (theResult != null) {
			if (theResult.getResultCode() == Constants.RETCODE_SUCCESS) {
				List<CrawlResult> theResultList = theResult.getResultList();
				if (theResultList != null && !theResultList.isEmpty()) {
					System.out.println("Crawled result of " + cInput + "...");
					for (CrawlResult crawlResult : theResultList) {
						System.out.println("-> " + crawlResult);
					}
				} else {
					System.out.println("Crawl process okay but list is empty/null!");
				}
			} else if (theResult.getResultCode() == Constants.RETCODE_NODATA) {
				System.out.println("Crawl process done but returned no result.");
			} else {
				System.err.println("Crawl process got error! Detail : ["+theResult.getResultCode()+"] - " + theResult.getResultMsg());
			}
		} else {
			System.err.println("Result object is null!");
		}
		System.exit(0);
	}
}
