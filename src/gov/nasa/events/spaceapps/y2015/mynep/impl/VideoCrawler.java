package gov.nasa.events.spaceapps.y2015.mynep.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.ResourceId;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.Thumbnail;

import gov.nasa.events.spaceapps.y2015.mynep.IVideoCrawl;
import gov.nasa.events.spaceapps.y2015.mynep.pojos.VideoProcessResult;
import gov.nasa.events.spaceapps.y2015.mynep.pojos.VideoResultPojo;
import gov.nasa.events.spaceapps.y2015.mynep.utils.Auth;
import gov.nasa.events.spaceapps.y2015.mynep.utils.Constants;

public class VideoCrawler implements IVideoCrawl {

	@Override
	public VideoProcessResult getVideoResult(String qKey) {
		VideoProcessResult returnValue = new VideoProcessResult();
		try {
            // This object is used to make YouTube Data API requests. The last
            // argument is required, but since we don't need anything
            // initialized when the HttpRequest is initialized, we override
            // the interface and provide a no-op function.
            YouTube youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, Auth.JSON_FACTORY, new HttpRequestInitializer() {
                public void initialize(HttpRequest request) throws IOException {
                }
            }).setApplicationName(Constants.GAPI_APPNAME).build();

            // Prompt the user to enter a query term.
            String queryTerm = qKey;

            // Define the API request for retrieving search results.
            YouTube.Search.List search = youtube.search().list("id,snippet");

            // Set your developer key from the Google Developers Console for
            // non-authenticated requests. See:
            // https://console.developers.google.com/
            //String apiKey = properties.getProperty("youtube.apikey");
            search.setKey(Constants.GAPI_KEY);
            search.setQ(queryTerm);

            // Restrict the search results to only include videos. See:
            // https://developers.google.com/youtube/v3/docs/search/list#type
            search.setType("video");

            // To increase efficiency, only retrieve the fields that the
            // application uses.
            search.setFields("items(id/kind,id/videoId,snippet/title,snippet/thumbnails/default/url)");
            search.setMaxResults(new Long(Constants.GAPI_NUMBEROFVIDEORESULTS));

            // Call the API and print results.
            SearchListResponse searchResponse = search.execute();
            List<SearchResult> searchResultList = searchResponse.getItems();
            if (searchResultList != null && !searchResultList.isEmpty()) {
            	List<VideoResultPojo> retList = new ArrayList<VideoResultPojo>();
            	Iterator<SearchResult> iteratorSearchResults =searchResultList.iterator();
            	while (iteratorSearchResults.hasNext()) {
                    SearchResult singleVideo = iteratorSearchResults.next();
                    ResourceId rId = singleVideo.getId();
                    // Confirm that the result represents a video. Otherwise, the
                    // item will not contain a video ID.
                    if (rId.getKind().equals("youtube#video")) {
                        Thumbnail thumbnail = singleVideo.getSnippet().getThumbnails().getDefault();
                        retList.add(new VideoResultPojo(rId.getVideoId(), singleVideo.getSnippet().getTitle(), thumbnail.getUrl()));
                    }
                }
            	returnValue.setVideoResultList(retList);
            	returnValue.setResultCode(Constants.RETCODE_SUCCESS);
            	returnValue.setResultMsg("Success");
            } else {
            	returnValue.setResultCode(Constants.RETCODE_NODATA);
            	returnValue.setResultMsg("No data for query key");
            }
        } catch (GoogleJsonResponseException e) {
            System.err.println("There was a service error: " + e.getDetails().getCode() + " : " + e.getDetails().getMessage());
            e.printStackTrace();
            returnValue.setResultCode(Constants.RETCODE_FAIL);
        	returnValue.setResultMsg("Json response exception! Detail : " + e.getMessage());
        } catch (IOException e) {
            System.err.println("There was an IO error: " + e.getCause() + " : " + e.getMessage());
            e.printStackTrace();
            returnValue.setResultCode(Constants.RETCODE_FAIL);
        	returnValue.setResultMsg("IO error! Detail : " + e.getMessage());
        } catch (Throwable t) {
        	returnValue.setResultCode(Constants.RETCODE_FAIL);
        	returnValue.setResultMsg("System fault! Detail : " + t.getMessage());
            t.printStackTrace();
        }
		return returnValue;
	}
}
