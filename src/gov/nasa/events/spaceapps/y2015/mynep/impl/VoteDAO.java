package gov.nasa.events.spaceapps.y2015.mynep.impl;

import gov.nasa.events.spaceapps.y2015.mynep.IVoteADO;
import gov.nasa.events.spaceapps.y2015.mynep.pojos.CrawlResult;
import gov.nasa.events.spaceapps.y2015.mynep.pojos.VotePojo;
import gov.nasa.events.spaceapps.y2015.mynep.utils.Constants;
import gov.nasa.events.spaceapps.y2015.mynep.utils.DbUtils;

import java.sql.ResultSet;
import java.util.List;

public class VoteDAO implements IVoteADO {

	public VoteDAO() {
		dUtils = new DbUtils();
	}
	
	@Override
	public VotePojo recordNewVote(VotePojo vPojo) throws Exception {
		VotePojo returnValue = vPojo;
		try {
			String iQuery = "insert into t_votes (eventIdTx, voteVal) values('"+vPojo.getUniquIdentifierTxt()+"', "+vPojo.getVoteGeneralVal()+")";
			boolean retVal = dUtils.executeUpdate(iQuery);
			if (retVal) {
				dUtils.dispose();
				CrawlResult cResult = new CrawlResult();
				cResult.setResultUniqueIdentifier(vPojo.getUniquIdentifierTxt());
				VotePojo tResult = getVoteOfCrawledElement(cResult);
				returnValue.setResultCode(Constants.RETCODE_SUCCESS);
				returnValue.setResultMsg("Success");
				returnValue.setVoteGeneralVal(tResult.getVoteGeneralVal());
			} else {
				returnValue.setResultCode(Constants.RETCODE_NODATA);
				returnValue.setResultMsg("no data was added");
			}
		} catch (Exception e) {
			returnValue.setResultCode(Constants.RETCODE_FAIL);
			returnValue.setResultMsg("Exception while recording new vote! Detail : " + e.getMessage());
			throw e;
		} finally {
			try {
				dUtils.dispose();
			} catch (Exception e) {
				throw e;
			}
		}
		return returnValue;
	}

	@Override
	public VotePojo getVoteOfCrawledElement(CrawlResult cResult) throws Exception {
		VotePojo returnValue = new VotePojo();
		try {
			String qQuery = "SELECT sum(voteVal)/count(eventIdTx) as vx FROM t_votes WHERE eventIdTx = '"+cResult.getResultUniqueIdentifier()+"'";
			ResultSet rs = dUtils.executeQuery(qQuery);
			if (rs != null) {
				if (rs.next()) {
					returnValue.setResultCode(Constants.RETCODE_SUCCESS);
					returnValue.setResultMsg("Success");
					returnValue.setUniquIdentifierTxt(cResult.getResultUniqueIdentifier());
					returnValue.setVoteGeneralVal(rs.getDouble("vx"));
				}
			}
		} catch (Exception e) {
			returnValue.setResultCode(Constants.RETCODE_FAIL);
			returnValue.setResultMsg("Exception while recording new vote! Detail : " + e.getMessage());
			throw e;
		} finally {
			try {
				dUtils.dispose();
			} catch (Exception e) {
				throw e;
			}
		}
		return returnValue;
	}

	@Override
	public List<CrawlResult> getBulkVoteList(List<CrawlResult> crawlList) throws Exception {
		List<CrawlResult> returnValue = crawlList;
		try {
			String qQuery = "select eventIdTx, sum(voteVal)/count(eventIdTx) as vx from t_votes where eventIdTx in (";
			if (crawlList != null && !crawlList.isEmpty()) {
				for (CrawlResult crawl : crawlList) {
					qQuery += "'" + crawl.getResultUniqueIdentifier() + "', ";
				}
				qQuery += "'1') group by eventIdTx";
			}
			ResultSet rs = dUtils.executeQuery(qQuery);
			if (rs != null) {
				while (rs.next()) {
					setVotesOfRelatedEvent(crawlList, new VotePojo(rs.getDouble("vx"), rs.getString("eventIdTx")));
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				dUtils.dispose();
			} catch (Exception e) {
				throw e;
			}
		}
		return returnValue;
	}
	
	// Set the realted events vote...
	private void setVotesOfRelatedEvent(List<CrawlResult> cList, VotePojo vPojo) {
		for (int i = 0;i<=cList.size() - 1;i++) {
			if (cList.get(i).getResultUniqueIdentifier().trim().equals(vPojo.getUniquIdentifierTxt())) {
				cList.get(i).setVoteInfo(vPojo);
				break;
			}
		}
	}
	
	// Fields...
	private DbUtils dUtils;
}
