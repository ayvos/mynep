package gov.nasa.events.spaceapps.y2015.mynep;

import gov.nasa.events.spaceapps.y2015.mynep.pojos.CrawlInput;
import gov.nasa.events.spaceapps.y2015.mynep.pojos.CrawlProcessResult;

public interface ICrawler {

	public CrawlProcessResult doCrawlProcess(CrawlInput cInput);
}
