package gov.nasa.events.spaceapps.y2015.mynep.utils;

public class Constants {

	// NASA base data constants...
	public static final String NASA_SITE_PREFIX 								= "http://earthobservatory.nasa.gov";
	public static final String NASA_SITE_PREFIX_IMAGES 							= "/Images/category.php";
	public static final String NASA_SITE_PREFIX_PARAM_CATEGORY 					= "cat_id";
	public static final String NASA_SITE_PREFIX_PARAM_MONTH 					= "m";
	public static final String NASA_SITE_PREFIX_PARAM_YEAR 						= "y";
	
	// Html image based values...
	public static final String NASA_SITE_PREFIX_HTML_CONTENT_TODAY_IMAGE 		= "/IOTD";
	public static final String NASA_SITE_PREFIX_HTML_CONTENT_NATURAL_HAZARD 	= "/NaturalHazards";
	
	// Sys return code constants...
	public static final int RETCODE_SUCCESS 									= 100;
	public static final int RETCODE_NODATA 										= 101;
	public static final int RETCODE_FAIL 										= 102;
	
	// Html form param names...
	public static final String HTML_FORM_PARAM_CATEGORY							= "t_c";
	public static final String HTML_FORM_PARAM_MONTH							= "t_m";
	public static final String HTML_FORM_PARAM_YEAR								= "t_y";
	public static final String HTML_FORM_PARAM_SUBJECT_ID						= "sId";
	public static final String HTML_FORM_PARAM_VOTE_VALUE						= "vVal";
	
	// Session fields...
	public static final String HTML_SESSION_CRAWL_PROCESS_RESULT				= "CrawlResult";
	public static final String HTML_SESSION_CRAWL_INPUT							= "CrawlInput";
	
	// Google Api Key
	public static final String GAPI_KEY											= "AIzaSyBirBpy73-onJukdQkfvUPwPYUAEyKZJho";
	public static final String GAPI_APPNAME										= "mynep";
	public static final int GAPI_NUMBEROFVIDEORESULTS							= 1;
	public static final String GAPI_WATCH_SITE_PREVIX							= "https://www.youtube.com/watch?v=";
	
	// Vote value ranges and messages...
	public static final double VOTE_RANGE_PHASE_0_VAL							= 0;
	public static final double VOTE_RANGE_PHASE_1_VAL							= 1.25d;
	public static final double VOTE_RANGE_PHASE_2_VAL							= 2.0d;
	
	public static final String VOTE_RANGE_PHASE_0_TXT							= "No votes...:(";
	public static final String VOTE_RANGE_PHASE_1_TXT							= "Nature we know";
	public static final String VOTE_RANGE_PHASE_2_TXT							= "Make me feel exited";
	public static final String VOTE_RANGE_PHASE_OVER							= "Oh my cosmos!";
	
	// Db constants...
	public static final String DB_CONS_HOST										= "159.8.128.101";
	public static final int DB_CONS_PORT										= 3307;
	public static final String DB_CONS_DBNAME									= "ddf80eed719664f259be1f7c7758245d4";
	public static final String DB_CONS_USER										= "uzt6D0PkOUlRE";
	public static final String DB_CONS_PSWD										= "p3BaHnWBbYGdX";
}

