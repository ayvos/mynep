package gov.nasa.events.spaceapps.y2015.mynep.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;



public class DbUtils {

	public DbUtils(){
	}
	
	// Connection returner...
	private void openConn() throws Exception {
		try {
			String conStr = "jdbc:mysql://" + Constants.DB_CONS_HOST + ":" + Constants.DB_CONS_PORT + "/" + Constants.DB_CONS_DBNAME;
			conn = DriverManager.getConnection(conStr, Constants.DB_CONS_USER, Constants.DB_CONS_PSWD);
			conn.setAutoCommit(true);
			System.out.println("Get conn. from db! Is okay : " + !conn.isClosed());
		} catch (Exception e) {
			throw e;
		}
	}
	
	// Statement returner...
	private Statement getStatement() throws Exception {
		try {
			openConn();
			if (conn != null && !conn.isClosed()) {
				stat = conn.createStatement();
			} else {
				throw new Exception("Connection is not null but seems closed!");
			}
		} catch (Exception e) {
			throw e;
		}
		return stat;
	}
	
	public boolean executeUpdate(String query) throws Exception {
		boolean returnValue = false;
		try {
			returnValue = getStatement().executeUpdate(query) > 0 ? true : false;
		} catch (Exception e) {
			throw e;
		}
		return returnValue;
	}
	
	public ResultSet executeQuery(String query) throws Exception {
		try {
			rs = getStatement().executeQuery(query);
		} catch (Exception e) {
			throw e;
		}
		return rs;
	}
	
	// Connection disposer...
	public boolean dispose() throws Exception {
		boolean returnValue = false;
		try {
			if (conn != null) {
				if (rs != null) {
					rs.close();
				}
				if (stat != null && !stat.isClosed()) {
					if (stat.getResultSet() != null && !stat.getResultSet().isClosed()) {
						stat.getResultSet().close();
					}
					stat.close();
				}
				conn.close();
			}
		} catch (Exception e) {
			throw e;
		}
		return returnValue;
	}
	
	// Fields...
	private Connection conn;
	private Statement stat;
	private ResultSet rs;
}
