package gov.nasa.events.spaceapps.y2015.mynep;

import gov.nasa.events.spaceapps.y2015.mynep.pojos.CrawlResult;
import gov.nasa.events.spaceapps.y2015.mynep.pojos.VotePojo;

import java.util.List;

public interface IVoteADO {

	public VotePojo recordNewVote(VotePojo vPojo) throws Exception;
	public VotePojo getVoteOfCrawledElement(CrawlResult cResult) throws Exception;
	public List<CrawlResult> getBulkVoteList(List<CrawlResult> crawlList) throws Exception;
}
