package gov.nasa.events.spaceapps.y2015.mynep.pojos;

import java.util.List;

public class VideoProcessResult extends GenericObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8708343444408172712L;

	
	// Accessor methods...
	public List<VideoResultPojo> getVideoResultList() {
		return videoResultList;
	}

	public void setVideoResultList(List<VideoResultPojo> videoResultList) {
		this.videoResultList = videoResultList;
	}

	// Fields...
	private List<VideoResultPojo> videoResultList;
}
