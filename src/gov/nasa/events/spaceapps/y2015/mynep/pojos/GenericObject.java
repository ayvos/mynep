package gov.nasa.events.spaceapps.y2015.mynep.pojos;

import java.io.Serializable;

public class GenericObject implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1743053417434438226L;
	
	// Accessor methods...
	public int getResultCode() {
		return resultCode;
	}
	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	
	// Fields...
	private int resultCode;
	private String resultMsg;
}
