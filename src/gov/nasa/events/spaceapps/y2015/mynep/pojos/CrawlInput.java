package gov.nasa.events.spaceapps.y2015.mynep.pojos;

import java.io.Serializable;

public class CrawlInput implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1842394837055970416L;
	
	// Accessor methods...
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	
	// Fields...
	private String categoryId;
	private String month;
	private String year;
}
