package gov.nasa.events.spaceapps.y2015.mynep.pojos;

import java.io.Serializable;
import java.util.List;

public class VoteResult extends GenericObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6527974102875326285L;

	// Accessor methods...
	public List<VotePojo> getVoteResults() {
		return voteResults;
	}

	public void setVoteResults(List<VotePojo> voteResults) {
		this.voteResults = voteResults;
	}
	
	// Fields...
	private List<VotePojo> voteResults;
}
