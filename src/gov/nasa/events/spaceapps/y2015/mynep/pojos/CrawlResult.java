package gov.nasa.events.spaceapps.y2015.mynep.pojos;

import gov.nasa.events.spaceapps.y2015.mynep.utils.Constants;

public class CrawlResult extends GenericObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 419111696109282899L;

	public CrawlResult(){
	}

	// Accessor methods...
	public CrawlResultType getResultType() {
		return resultType;
	}
	public String getResultUrl() {
		return resultUrl;
	}
	public void setResultUrl(String resultUrl) {
		this.resultUrl = resultUrl;
		if (resultUrl != null && resultUrl.trim().length() > 0) {
			this.resultUniqueIdentifier = "" + resultUrl.hashCode();
		}
	}
	public void setResultType(CrawlResultType resultType) {
		this.resultType = resultType;
	}
	public String getResultCoreTxt() {
		return resultCoreTxt;
	}
	public void setResultCoreTxt(String resultCoreTxt) {
		this.resultCoreTxt = resultCoreTxt;
	}
	public String getResultRecordDate() {
		return resultRecordDate;
	}

	public void setResultRecordDate(String resultRecordDate) {
		this.resultRecordDate = resultRecordDate;
	}

	public String getResultImageUrl() {
		return resultImageUrl;
	}

	public void setResultImageUrl(String resultImageUrl) {
		this.resultImageUrl = resultImageUrl;
	}

	public String getResultUniqueIdentifier() {
		return resultUniqueIdentifier;
	}

	public void setResultUniqueIdentifier(String resultUniqueIdentifier) {
		this.resultUniqueIdentifier = resultUniqueIdentifier;
	}

	public VotePojo getVoteInfo() {
		return voteInfo;
	}

	public void setVoteInfo(VotePojo voteInfo) {
		this.voteInfo = voteInfo;
	}

	public String getVoteExpStr() {
		if (voteInfo == null) {
			this.voteInfo = new VotePojo();
		}
		voteInfo.setVoteExpr(Constants.VOTE_RANGE_PHASE_0_TXT);
		if (voteInfo.getVoteGeneralVal() <= Constants.VOTE_RANGE_PHASE_0_VAL) {
			voteInfo.setVoteExpr(Constants.VOTE_RANGE_PHASE_0_TXT);
		} else if (voteInfo.getVoteGeneralVal() > Constants.VOTE_RANGE_PHASE_0_VAL && voteInfo.getVoteGeneralVal() <= Constants.VOTE_RANGE_PHASE_1_VAL) {
			voteInfo.setVoteExpr(Constants.VOTE_RANGE_PHASE_1_TXT);
		} else if (voteInfo.getVoteGeneralVal() > Constants.VOTE_RANGE_PHASE_1_VAL && voteInfo.getVoteGeneralVal() < Constants.VOTE_RANGE_PHASE_2_VAL) {
			voteInfo.setVoteExpr(Constants.VOTE_RANGE_PHASE_2_TXT);
		} else {
			voteInfo.setVoteExpr(Constants.VOTE_RANGE_PHASE_OVER);
		}
		return this.voteInfo.getVoteExpr();
	}

	@Override
	public String toString() {
		return "-T : " + this.resultType + "\r\n -CT : " + this.resultCoreTxt + "\r\n -RU : " + this.resultUrl + "\r\n -RIU : " + this.resultImageUrl + " -@ : " + this.resultRecordDate;
	}
	
	@Override
	public int hashCode() {
		int returnValue = super.hashCode();
		if (this.resultUrl != null && this.resultUrl.trim().length() > 0) {
			returnValue = resultUrl.hashCode();
		}
		return returnValue;
	}

	@Override
	public boolean equals(Object obj) {
		boolean returnValue = false;
		CrawlResult comingObject = (CrawlResult)obj;
		if (this.resultUrl != null && comingObject.getResultUrl() != null && this.resultUrl.trim().equals(comingObject.getResultUrl().trim())) {
			returnValue = true;
		}
		return returnValue;
	}

	// Fields...
	private String resultUrl;
	private String resultCoreTxt;
	private String resultRecordDate;
	private String resultImageUrl;
	private String resultUniqueIdentifier;
	private CrawlResultType resultType;
	private VotePojo voteInfo = new VotePojo();
}
