package gov.nasa.events.spaceapps.y2015.mynep.pojos;

import java.io.Serializable;

public class VideoResultPojo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7798176709993880679L;

	public VideoResultPojo(String videoId, String videoTitle,
			String thumbnailPath) {
		super();
		this.videoId = videoId;
		this.videoTitle = videoTitle;
		this.thumbnailPath = thumbnailPath;
	}
	
	// Accessor methods...
	public String getVideoId() {
		return videoId;
	}
	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}
	public String getVideoTitle() {
		return videoTitle;
	}
	public void setVideoTitle(String videoTitle) {
		this.videoTitle = videoTitle;
	}
	public String getThumbnailPath() {
		return thumbnailPath;
	}
	public void setThumbnailPath(String thumbnailPath) {
		this.thumbnailPath = thumbnailPath;
	}
	
	// Fields...
	private String videoId;
	private String videoTitle;
	private String thumbnailPath;
}
