package gov.nasa.events.spaceapps.y2015.mynep.pojos;

import gov.nasa.events.spaceapps.y2015.mynep.utils.Constants;

public class VotePojo extends GenericObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7031429935032406897L;

	public VotePojo() {
	}
	
	public VotePojo(double voteGeneralVal, String uniquIdentifierTxt) {
		super();
		this.voteGeneralVal = voteGeneralVal;
		this.uniquIdentifierTxt = uniquIdentifierTxt;
	}

	// Accessor methods...
	public double getVoteGeneralVal() {
		return voteGeneralVal;
	}

	public void setVoteGeneralVal(double voteGeneralVal) {
		this.voteGeneralVal = voteGeneralVal;
		setVoteExpr(Constants.VOTE_RANGE_PHASE_0_TXT);
		if (voteGeneralVal <= Constants.VOTE_RANGE_PHASE_0_VAL) {
			setVoteExpr(Constants.VOTE_RANGE_PHASE_0_TXT);
		} else if (voteGeneralVal > Constants.VOTE_RANGE_PHASE_0_VAL && voteGeneralVal <= Constants.VOTE_RANGE_PHASE_1_VAL) {
			setVoteExpr(Constants.VOTE_RANGE_PHASE_1_TXT);
		} else if (voteGeneralVal > Constants.VOTE_RANGE_PHASE_1_VAL && voteGeneralVal < Constants.VOTE_RANGE_PHASE_2_VAL) {
			setVoteExpr(Constants.VOTE_RANGE_PHASE_2_TXT);
		} else {
			setVoteExpr(Constants.VOTE_RANGE_PHASE_OVER);
		}
	}

	public String getUniquIdentifierTxt() {
		return uniquIdentifierTxt;
	}

	public void setUniquIdentifierTxt(String uniquIdentifierTxt) {
		this.uniquIdentifierTxt = uniquIdentifierTxt;
	}

	public String getVoteExpr() {
		return voteExpr;
	}

	public void setVoteExpr(String voteExpr) {
		this.voteExpr = voteExpr;
	}


	// Fields...
	private double voteGeneralVal = 0.00d;
	private String uniquIdentifierTxt;
	private String voteExpr;
}
