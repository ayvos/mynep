package gov.nasa.events.spaceapps.y2015.mynep.pojos;

import java.util.List;

public class CrawlProcessResult extends GenericObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9024485614773769397L;
	
	// Accessor methods...
	public List<CrawlResult> getResultList() {
		return resultList;
	}

	public void setResultList(List<CrawlResult> resultList) {
		this.resultList = resultList;
	}
	
	// Fields...
	private List<CrawlResult> resultList;
}
