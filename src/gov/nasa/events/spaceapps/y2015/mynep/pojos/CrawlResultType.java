package gov.nasa.events.spaceapps.y2015.mynep.pojos;

public enum CrawlResultType {

	HAZARD_TEXT,
	HAZARD_IMAGE,
	HAZARD_URL,
	TODAY_TEXT,
	TODAY_IMAGE,
	TODAY_URL
}
